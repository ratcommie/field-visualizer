from flask import Flask, url_for, redirect
from matplotlib import pyplot
import numpy
from numpy import radians
import electrostatics
from electrostatics import PointCharge, ElectricField, GaussianCircle, LineCharge
from electrostatics import finalize_plot
import io
import base64
import tkinter

app = Flask(__name__)

# The area of interest
XMIN, XMAX = -40, 40
YMIN, YMAX = -30, 30
ZOOM = 6
XOFFSET = 0

@app.route('/')
def index():
	return """
<!DOCTYPE html>
<html>
  <body>
    <header>
      <div class="container">
        <h1 class="logo">Field Visualizer App</h1>
        <strong><nav>
          <ul class="menu">
            <li><a href="{{ redirect('/dipole') }}">Dipole</a></li>
            <li><a href="{{ redirect('/two-pos') }}">Two Positive Charges</a></li>
            <li><a href="{{ redirect('/line-point')}}">Line and Point Charge</a></li>
            <li><a href="{{ redirect('/line-line')}}">Two Line Charges</a></li>
          </ul>
        </nav></strong>
      </div>
    </header>
  </body>
</html>"""

@app.route('/dipole')
def dipole():
	electrostatics.init(XMIN, XMAX, YMIN, YMAX, ZOOM, XOFFSET)

	# Set up the charges and electric field
	charges = [PointCharge(1, [-1, 0]),
	           PointCharge(-1, [1, 0])]
	field = ElectricField(charges)

	# Set up the Gaussian surface
	g = GaussianCircle(charges[0].x, 0.1)

	# Create the field lines
	fieldlines = []
	for x in g.fluxpoints(field, 12):
	    fieldlines.append(field.line(x))
	fieldlines.append(field.line([10, 0]))

	# Create the vector grid
	x, y = numpy.meshgrid(numpy.linspace(XMIN/ZOOM+XOFFSET, XMAX/ZOOM+XOFFSET, 41),
	                      numpy.linspace(YMIN/ZOOM, YMAX/ZOOM, 31))
	u, v = numpy.zeros_like(x), numpy.zeros_like(y)
	n, m = x.shape
	for i in range(n):
	    for j in range(m):
	        if any(numpy.isclose(electrostatics.norm(charge.x-[x[i, j], y[i, j]]),
	                             0) for charge in charges):
	            u[i, j] = v[i, j] = None
	        else:
	            mag = field.magnitude([x[i, j], y[i, j]])**(1/5)
	            a = field.angle([x[i, j], y[i, j]])
	            u[i, j], v[i, j] = mag*numpy.cos(a), mag*numpy.sin(a)

	## Plotting ##

	# Field lines
	fig = pyplot.figure(figsize=(6, 4.5))
	field.plot()
	for fieldline in fieldlines:
	    fieldline.plot()
	for charge in charges:
	    charge.plot()
	finalize_plot()
	#fig.savefig('dipole-field-lines.pdf', transparent=True)

	# Field vectors
	fig = pyplot.figure(figsize=(6, 4.5))
	cmap = pyplot.cm.get_cmap('plasma')
	pyplot.quiver(x, y, u, v, pivot='mid', cmap=cmap, scale=35)
	for charge in charges:
	    charge.plot()
	finalize_plot()
	#fig.savefig('dipole-field-vectors.pdf', transparent=True)

	img = io.BytesIO()

	fig.savefig(img, format="png")
	img.seek(0)

	plot_url = base64.b64encode(img.getvalue()).decode()

	return '<img src="data:image/png;base64,{}">'.format(plot_url)

@app.route('/two-pos')
def two_positive_charges():
	electrostatics.init(XMIN, XMAX, YMIN, YMAX, ZOOM, XOFFSET)

	# Set up the charges and electric field
	charges = [PointCharge(1, [-2, 0]),
	           PointCharge(1, [2, 0]),
	           PointCharge(0, [0, 0])]
	field = ElectricField(charges)

	# Set up the Gaussian surfaces
	g = [GaussianCircle(charges[i].x, 0.1) for i in range(len(charges))]
	g[0].a0 = radians(-180)

	# Create the field lines
	fieldlines = []
	for g_ in g[:-1]:
	    for x in g_.fluxpoints(field, 12):
	        fieldlines.append(field.line(x))

	# Plotting
	fig = pyplot.figure(figsize=(6, 4.5))
	field.plot()
	for fieldline in fieldlines:
	    fieldline.plot()
	for charge in charges:
	    charge.plot()
	finalize_plot()

	img = io.BytesIO()

	fig.savefig(img, format="png")
	img.seek(0)

	plot_url = base64.b64encode(img.getvalue()).decode()

	return '<img src="data:image/png;base64,{}">'.format(plot_url)

@app.route('/line-point')
def line_point():
	electrostatics.init(XMIN, XMAX, YMIN, YMAX, ZOOM, XOFFSET)

	# Set up the charges and electric field
	charges = [LineCharge(1, [-1, -2], [-1, 2]), PointCharge(-1, [1, 0])]
	field = ElectricField(charges)

	# Set up the Gaussian surfaces
	g = GaussianCircle(charges[1].x, 0.1)

	# Create the field lines
	fieldlines = []
	for x in g.fluxpoints(field, 12):
	    fieldlines.append(field.line(x))
	fieldlines.append(field.line([-10, 0]))

	# Plotting
	fig = pyplot.figure(figsize=(6, 4.5))
	field.plot()
	for fieldline in fieldlines:
	    fieldline.plot()
	for charge in charges:
	    charge.plot()
	finalize_plot()

	img = io.BytesIO()

	fig.savefig(img, format="png")
	img.seek(0)

	plot_url = base64.b64encode(img.getvalue()).decode()

	return '<img src="data:image/png;base64,{}">'.format(plot_url)

@app.route('/line-line')
def line_line():
	electrostatics.init(XMIN, XMAX, YMIN, YMAX, ZOOM, XOFFSET)

	# Set up the charges and electric field
	a = 3
	charges = [LineCharge(1, [-0.5, -a], [-0.5, a]),
	           LineCharge(-1, [0.5, -a], [0.5, a])]
	field = ElectricField(charges)

	# Create the flux points manually
	fluxpoints = []
	fluxpoints += list([-0.51, y] for y in numpy.linspace(-a, a, 7)[1:-1])
	fluxpoints += list([-0.49, y] for y in numpy.linspace(-a, a, 7)[1:-1])
	fluxpoints += [[-0.5, -a-0.01], [-0.5, a+0.01]]

	# Create the field lines
	fieldlines = []
	for x in fluxpoints:
	    fieldlines.append(field.line(x))
	fieldlines.append(field.line([10, 0]))

	# Plotting
	fig = pyplot.figure(figsize=(6, 4.5))
	field.plot()
	for fieldline in fieldlines:
	    fieldline.plot()
	for charge in charges:
	    charge.plot()
	finalize_plot()

	img = io.BytesIO()

	fig.savefig(img, format="png")
	img.seek(0)

	plot_url = base64.b64encode(img.getvalue()).decode()

	return '<img src="data:image/png;base64,{}">'.format(plot_url)

if __name__ == '__main__':
	app.run()